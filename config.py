cfg = dict()

# PYTHON DEBUGGING
cfg['android mode'] = True

# RELEASE
cfg['version'] = 'dev build 5'

# CONNECTION
cfg['broker'] = 'broker.hivemq.com'
cfg['port'] = 1883
cfg['keepalive'] = 10
cfg['default thread'] = 'blackzone/test2/'
cfg['default user'] = 'user'
cfg['default password'] = 'password'

cfg['DES key'] = 'weednoseweednose'

cfg['connection file'] = 'data/connection.bz'
cfg['connection file android'] = 'connection.bz' #TODO: resolve this

# MENUS
cfg['menu margins'] = (0.15, 0.15)
cfg['banner size hint'] = (1, 0.3)
cfg['logo banner'] = 'img/blackzone_v3_small.png'

# HELLOSCREEN
cfg['fade in duration'] = 1
cfg['fade-slide interval'] = 0
cfg['slide duration'] = 1.5
cfg['final freeze'] = 0

# MENUSCREEN
cfg['startscreen button width'] = 0.4
cfg['app info'] = '''This is the newest blackzone build
Cool and sleek now, isnt it?
It will get even better, just wait.
Build 5, pre tech demo'''

# MAPSCREEN

cfg['button default'] = 'img/buttons/non_depressed3.png'
cfg['button down'] = 'img/buttons/depressed3.png'

cfg['listing height'] = 50


########################################
cfg['menu size hint'] = (1 - cfg['menu margins'][0], 1 - cfg['menu margins'][1])
cfg['mapscreen size hint'] = (1, 1)
cfg['button size hint'] = (1, 1 - cfg['banner size hint'][1])
cfg['tick interval'] = 1







