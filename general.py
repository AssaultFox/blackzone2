from pyDes import *
from config import cfg
from kivy.utils import platform
import os

def encrypt_password(password, key):

    bytehash = triple_des(key).encrypt(password, padmode=2)
    bytelist = list(bytehash)
    hash = str(bytelist[0])

    for item in bytelist[1:]:

        hash = hash + '-' + str(item)

    return hash


def decrypt_password(hash, key):

    stringlist = hash.split('-')
    intlist = []

    for item in stringlist:
        
        intlist.append(int(item))

    bytelist = bytes(intlist)
    password = triple_des(key).decrypt(bytelist, padmode=2)

    return password


def parse_connection_settings(app):

    if platform == 'android':
        from android.storage import primary_external_storage_path
        #fname = os.path.join(primary_external_storage_path(), cfg['connection file android'])
        directory = app.user_data_dir
        fname = os.path.join(directory, cfg['connection file android'])

    else:
        fname = cfg['connection file']

    try:
        f = open(fname, 'r')

    except:
        first(app)
        f = open(fname, 'r')

    content = f.read()

    s = content.split('\n')
    return {'user': s[0],
            'password': decrypt_password(s[1], cfg['DES key'])}


def first(app):
    
    s = cfg['default user'] + '\n'
    s = s + str(encrypt_password(cfg['default password'], cfg['DES key']))
    content = bytes(s, 'utf-8')


    if platform == 'android':
        from android.storage import primary_external_storage_path
        #fname = os.path.join(primary_external_storage_path(), cfg['connection file android'])
        directory = app.user_data_dir
        fname = os.path.join(directory, cfg['connection file android'])
        
    else:
        fname = cfg['connection file']

    f = open(fname, 'wb')
    f.write(content)