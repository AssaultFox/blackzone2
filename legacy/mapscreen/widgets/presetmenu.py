from typing import Text
from kivy import app
from kivy.app import App
from kivy.core import image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.graphics import Color, Ellipse, Line, Rectangle, Triangle
from kivy.app import App
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.utils import platform
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget

from kivy.uix.label import Label

from blackwidgets import Window
from blackwidgets import BlackzoneSensor


ROOT = None
A = None

def first():

    s = '<SLOT 1>\nPRESET 1\ndangersite.jpg\n#\n'
    s = s + '<SLOT 2>\nPRESET 2\ndangersite.jpg\n#\n'
    s = s + '<SLOT 3>\nPRESET 3\ndangersite.jpg\n#\n'
    s = s + '<SLOT 4>\nPRESET 4\ndangersite.jpg'
    content = bytes(s, 'utf-8')

    f = open('presets.bz', 'wb')
    f.write(content)


def parse_presets():

    if platform == 'android':
        from android.storage import app_storage_path
        fname = os.path.join(app_storage_path(), 'presets.bz')

    else:
        fname = 'presets.bz'

    try:
        f = open(fname, 'r')

    except:
        first()
        f = open(fname, 'r')

    finally:
        pass

    content = f.read()

    s = content.split('\n#\n')

    slots = []

    for item in s:

        print(item)
        lines = item.split('\n')
        print(lines)
        d = {}
        d['slot'] = lines[0]
        d['name'] = lines[1]
        d['image'] = lines[2]
        d['sensors'] = lines[3:]
        slots.append(d)

    return slots


def compile_presets(presets):

    s = str()

    for preset in presets:

        s = s + preset['slot'] + '\n'
        s = s + preset['name'] + '\n'
        s = s + preset['image'] + '\n'

        for sensor in preset['sensors']:

            s = s + sensor + '\n'
        
        s = s + '#\n'


    return s[:-3]


def on_cancel():

    global ROOT

    ROOT.remove_widget(ROOT.presetmenu)
    ROOT.presetmenu = None
    ROOT.presetmenu_open = False


def build_on_save(slot):

    global ROOT

    def fun():

        s = ROOT.mapscreen.map.source
        for item in ROOT.mapscreen.map.children:

            s = s + '\n' + item.address
            s = s + '-' + str(item.x)
            s = s + '-' + str(item.y)
            s = s + '-' + str(item.center_x)
            s = s + '-' + str(item.center_y)

        content = bytes(s, 'utf-8')

        if platform == 'android':
            from android.storage import app_storage_path
            fname = os.path.join(app_storage_path(),'saved-' + slot)
            
            with open(fname, 'wb') as f:
                f.write(content)

        else:
            fname = 'saved-' + slot
            
            with open(fname, 'wb') as f:
                f.write(content)

    return fun


def build_on_save2(slot, parent):

    global ROOT

    def fun():

        #get the contents of the presets.bz file

        content = parse_presets()

        d = {}
        d['slot'] = '<SLOT ' + str(slot) + '>'
        d['name'] = parent.name_field.text
        d['image'] = ROOT.mapscreen.map.source
        sensors = []
    
        for item in ROOT.mapscreen.map.children:

            s = item.address
            s = s + '-' + str(item.x)
            s = s + '-' + str(item.y)
            s = s + '-' + str(item.center_x)
            s = s + '-' + str(item.center_y)
            sensors.append(s)
        
        d['sensors'] = sensors

        content[slot - 1] = d

        string = compile_presets(content)

        stringb = bytes(string, 'utf-8')

        if platform == 'android':
            from android.storage import app_storage_path
            fname = os.path.join(app_storage_path(), 'presets.bz')

        else:
            fname = 'presets.bz'
            
        with open(fname, 'wb') as f:
            f.write(stringb)

    return fun


def build_on_load2(slot):

    global ROOT

    def fun():
        
        presets = parse_presets()
        preset = presets[slot - 1]
        sensors = preset['sensors']
        sensors_ = []

        ROOT.mapscreen.map.on_reset()
    
        for line in sensors:

            sensor = []
            sensor_def = line.split('-')
            sensor.append(sensor_def[0])
            sensor.append(float(sensor_def[1]))
            sensor.append(float(sensor_def[2]))
            sensor.append(float(sensor_def[3]))
            sensor.append(float(sensor_def[4]))
            sensors_.append(sensor)

            ROOT.mapscreen.map.source = preset['image']

            for sensor in sensors_:

                btn = BlackzoneSensor(text ='', 
                                        size_hint = (0.05, 0.05), 
                                        pos = (sensor[1], sensor[2]))
                
                btn.address = sensor[0]

                ROOT.mapscreen.map.add_widget(btn, canvas = 'after')

                btn.draw_first((sensor[3], sensor[4]))

    return fun


def build_on_load(slot):

    global ROOT

    def fun():
        
        if platform == 'android':
            from android.storage import app_storage_path
            fname = os.path.join(app_storage_path(),'saved-' + slot)
            
            with open(fname, 'r') as f:
                content = f.read()

        else:
            fname = 'saved-' + slot
            
            with open(fname, 'r') as f:
                content = f.read()


            lines = content.split('\n')

            source = lines[0]
            sensors = []

            for line in lines[1:]:

                sensor = []
                sensor_def = line.split('-')
                sensor.append(sensor_def[0])
                sensor.append(float(sensor_def[1]))
                sensor.append(float(sensor_def[2]))
                sensor.append(float(sensor_def[3]))
                sensor.append(float(sensor_def[4]))
                sensors.append(sensor)

            print (sensors)

            ROOT.mapscreen.map.on_reset()

            ROOT.mapscreen.map.source = source

            for sensor in sensors:

                btn = BlackzoneSensor(text ='', 
                                        size_hint = (0.05, 0.05), 
                                        pos = (sensor[1], sensor[2]))
                
                btn.address = sensor[0]

                ROOT.mapscreen.map.add_widget(btn, canvas = 'after')

                btn.draw_first((sensor[3], sensor[4]))

    return fun


def buildPresetMenu(A_):

    global A 
    A = A_
    global ROOT
    ROOT = A.root

    names = []
    presets = parse_presets()
    for preset in presets:
        
        names.append(preset['name'])

    Frame = Window(orientation = 'vertical', source = 'img/black.png')
    Frame.allow_stretch = True
    Frame.keep_ratio = False

    PresetContainer = BoxLayout(orientation = 'horizontal')

    def buildPresetBox(name):

        Frame2 = Window(orientation = 'vertical', source = 'img/black.png')

        button_box = BoxLayout(size_hint = (1, 0.8))

        load_button = Button(text = 'Load')
        Frame2.load_button = load_button

        save_button = Button(text = 'Save')
        Frame2.save_button = save_button

        button_box.add_widget(load_button)
        button_box.add_widget(save_button)

        name_field = TextInput( text = name,
                                foreground_color = [1, 1, 1, 1],
                                background_color = [0.1, 0.1, 0.1 , 1],
                                size_hint = (1, 0.3))
        Frame2.name_field = name_field

        Frame2.add_widget(name_field)
        Frame2.add_widget(button_box)

        return Frame2

    PresetContainerLeft = BoxLayout(orientation = 'vertical')
    PresetContainerRight = BoxLayout(orientation = 'vertical')

    slot1 = buildPresetBox(names[0])
    Frame.slot1 = slot1
    PresetContainerLeft.add_widget(slot1)
    slot1.save_button.on_press = build_on_save2(1, slot1)
    slot1.load_button.on_press = build_on_load2(1)

    slot2 = buildPresetBox(names[1])
    Frame.slot2 = slot2
    PresetContainerLeft.add_widget(slot2)
    slot2.save_button.on_press = build_on_save2(2, slot2)
    slot2.load_button.on_press = build_on_load2(2)

    slot3 = buildPresetBox(names[2])
    Frame.slot3 = slot3
    PresetContainerRight.add_widget(slot3)
    slot3.save_button.on_press = build_on_save2(3, slot3)
    slot3.load_button.on_press = build_on_load2(3)

    slot4 = buildPresetBox(names[3])
    Frame.slot4 = slot4
    PresetContainerRight.add_widget(slot4)
    slot4.save_button.on_press = build_on_save2(4, slot4)
    slot4.load_button.on_press = build_on_load2(4)

    PresetContainer.add_widget(PresetContainerLeft)
    PresetContainer.add_widget(PresetContainerRight)

    cancel_button = Button(text = 'Cancel')
    cancel_button.size_hint = (1, 0.2)
    Frame.cancel_button = cancel_button
    cancel_button.on_release = on_cancel

    Frame.add_widget(PresetContainer)
    Frame.add_widget(cancel_button)


    return Frame