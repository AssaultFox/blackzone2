from typing import Text
from kivy import app
from kivy.app import App
from kivy.core import image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.graphics import Color, Ellipse, Line, Rectangle, Triangle
from kivy.app import App
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.utils import platform
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget

from kivy.uix.label import Label

from blackwidgets import Window


A = None
ROOT = None

def on_apply():

    ROOT.mapscreen.map.locked = False
    ROOT.mapscreen.map.add_active = True
    ROOT.mapscreen.map.add_ID = ROOT.addmenu.ID_input.text

    ROOT.remove_widget(ROOT.addmenu)
    ROOT.addmenu = None
    ROOT.addmenu_open = False
    

def on_cancel():

    ROOT.remove_widget(ROOT.addmenu)
    ROOT.addmenu = None
    ROOT.addmenu_open = False
    

def buildAddMenu(A_):

    global A 
    A = A_
    global ROOT
    ROOT = A.root

    Frame = Window(orientation = 'vertical', source = 'img/black.png')
    Frame.allow_stretch = True
    Frame.keep_ratio = False

    IDSubbox = BoxLayout(orientation = 'horizontal')

    ID_input = TextInput(text = 'kitchen')
    Frame.ID_input = ID_input
    IDSubbox.add_widget(Label(text = 'ID', size_hint = (0.25, 1)))
    IDSubbox.add_widget(ID_input)

    ButtonSubbox = BoxLayout(orientation = 'horizontal', size_hint = (1, 0.15))
    
    apply_button = Button(text = 'Apply')
    apply_button.on_press = on_apply
    Frame.apply_button = apply_button
    ButtonSubbox.add_widget(apply_button)

    cancel_button = Button(text = 'Cancel')
    cancel_button.on_press = on_cancel
    Frame.cancel_button = cancel_button
    ButtonSubbox.add_widget(cancel_button)

    ApplyButton = Button(text = 'Add')
    ApplyButton.background_color = (0, 1, 0, 1)

    Frame.add_widget(IDSubbox)
    Frame.add_widget(ButtonSubbox)

    return Frame