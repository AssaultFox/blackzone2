from typing import Text
from kivy import app
from kivy.app import App
from kivy.core import image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.graphics import Color, Ellipse, Line, Rectangle, Triangle
from kivy.app import App
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.utils import platform
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget

from kivy.uix.label import Label
from blackwidgets import Window

A = None
ROOT = None


def on_apply():

    chosen = ROOT.imgmenu.file_chooser.selection[0]
    ROOT.mapscreen.map.source = chosen

    ROOT.remove_widget(ROOT.imgmenu)
    ROOT.imgmenu = None
    ROOT.imgmenu_open = False


def on_cancel():

    ROOT.remove_widget(ROOT.imgmenu)
    ROOT.imgmenu = None
    ROOT.imgmenu_open = False


def on_debug():

    if platform == 'android':
        from android.permissions import request_permissions
        from android.permissions import Permission
        request_permissions([Permission.READ_EXTERNAL_STORAGE])


def buildImgMenu(A_):

    global A
    A = A_
    global ROOT
    ROOT = A.root

    Frame = Window(orientation = 'vertical', source = 'img/black.png')
    Frame.allow_stretch = True
    Frame.keep_ratio = False

    ButtonSubbox = BoxLayout(orientation = 'horizontal', size_hint = (1, 0.15))
    
    apply_button = Button(text = 'Apply')
    apply_button.on_press = on_apply
    Frame.apply_button = apply_button
    ButtonSubbox.add_widget(apply_button)

    cancel_button = Button(text = 'Cancel')
    cancel_button.on_press = on_cancel
    Frame.cancel_button = cancel_button
    ButtonSubbox.add_widget(cancel_button)

    debug_button = Button(text = '<DEBUG>')
    debug_button.on_press = on_debug
    Frame.debug_button = debug_button
    ButtonSubbox.add_widget(debug_button)

    file_chooser = FileChooserIconView()

    if platform == 'android':
        
        from android.storage import primary_external_storage_path
        file_chooser.path = primary_external_storage_path()

    Frame.file_chooser = file_chooser
    Frame.add_widget(file_chooser)
    Frame.add_widget(ButtonSubbox)
    return Frame