from typing import Text
from kivy import app
from kivy.app import App
from kivy.core import image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.graphics import Color, Ellipse, Line, Rectangle, Triangle
from kivy.app import App
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.utils import platform
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget

from kivy.uix.label import Label
from blackwidgets import Window


A = None
ROOT = None
    

def on_apply():

        broker_new = ROOT.appmenu.broker_input.text
        thread_new = ROOT.appmenu.thread_input.text
        port_new = ROOT.appmenu.port_input.text

        if broker_new != A.broker or thread_new != A.thread or port_new != A.port:
            ROOT.redefine_mqtt(broker_new, thread_new, port_new)

        ROOT.remove_widget(ROOT.appmenu)
        ROOT.appmenu = None
        ROOT.appmenu_open = False


def on_reset():

    ROOT.mapscreen.map.on_reset()


def buildAppMenu(A_):

    global A
    A = A_
    global ROOT
    ROOT = A.root

    Frame = Window(orientation = 'vertical', source = 'img/black.png')
    Frame.allow_stretch = True
    Frame.keep_ratio = False

    BrokerSubbox = BoxLayout(orientation = 'horizontal')
    broker_input = TextInput(text = A.broker)
    Frame.broker_input = broker_input
    BrokerSubbox.add_widget(Label(text = 'Broker', size_hint = (0.25, 1)))
    BrokerSubbox.add_widget(broker_input)

    ThreadSubbox = BoxLayout(orientation = 'horizontal')
    thread_input = TextInput(text = A.thread)
    Frame.thread_input = thread_input
    ThreadSubbox.add_widget(Label(text = 'Thread', size_hint = (0.25, 1)))
    ThreadSubbox.add_widget(thread_input)

    PortSubbox = BoxLayout(orientation = 'horizontal')
    port_input = TextInput(text = A.port)
    Frame.port_input = port_input
    PortSubbox.add_widget(Label(text = 'Port', size_hint = (0.25, 1)))
    PortSubbox.add_widget(port_input)

    ResetButton = Button(text = 'Reset map')
    ResetButton.on_release = on_reset

    ApplyButton = Button(text = 'Apply')
    ApplyButton.on_release = on_apply

    ApplyButton.background_color = (0, 1, 0, 1)

    Frame.add_widget(BrokerSubbox)
    Frame.add_widget(ThreadSubbox)
    Frame.add_widget(PortSubbox)
    Frame.add_widget(ResetButton)
    Frame.add_widget(ApplyButton)

    Frame.reset_button = ResetButton
    Frame.apply_button = ApplyButton

    return Frame