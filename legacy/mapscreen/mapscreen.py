from kivy.app import App
from kivy.utils import platform
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget
import os
from kivy.uix.label import Label
from kivy.core.window import Window

from .widgets import *

from blackwidgets import *

from kivy.clock import mainthread
import paho.mqtt.client as mqtt

ROOT = None
SCREEN = None

def build_mapscreen(screen, root):

    global ROOT, SCREEN

    ROOT = root
    SCREEN = screen

    screen.mapscreen = BoxLayout(orientation = 'horizontal')

    screen.mapscreen.menu = BoxLayout(orientation = 'vertical', size_hint = (.15, 1))
    screen.mapscreen.map = BlackzoneMap(source = 'dangersite.jpg', size_hint = (0.85, 1))

    #Appmenu button
    appmenu_button = Button(text = 'APPMENU')
    appmenu_button.on_press = root.on_appmenu
    screen.mapscreen.menu.appmenu_button = appmenu_button
    screen.mapscreen.menu.add_widget(appmenu_button)

    #Add button
    addmenu_button = Button(text = 'ADD')
    addmenu_button.on_press = root.on_addmenu
    screen.mapscreen.menu.addmenu_button = addmenu_button
    screen.mapscreen.menu.add_widget(addmenu_button)
    
    #Image button
    img_button = Button(text = 'IMAGE')
    img_button.on_press = root.on_imgmenu
    screen.mapscreen.menu.img_button = img_button
    screen.mapscreen.menu.add_widget(img_button)

    #Save/load button
    sl_button = Button(text = 'Save/Load')
    sl_button.on_press = root.on_preset_menu
    screen.mapscreen.menu.sl_button = sl_button
    screen.mapscreen.menu.add_widget(sl_button)

    screen.mapscreen.add_widget(screen.mapscreen.menu)
    screen.mapscreen.add_widget(screen.mapscreen.map)

    screen.appmenu_open = False
    screen.imgmenu_open = False
    screen.addmenu_open = False
    screen.presetmenu_open = False