from typing import Text
from kivy.app import App
from kivy.utils import platform
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.uix.label import Label
from config import cfg
from kivy.animation import Animation, AnimationTransition
import os
from general import *

from kivy.uix.scrollview import ScrollView

from kivy.clock import mainthread


class MapScreen(BoxLayout):

    def __init__(self, app):


        super().__init__(   size_hint = cfg['menu size hint'], 
                            orientation = 'horizontal',
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

        self.app = app
        self.buttonmenu = MapScreen_ButtonMenu(self.app)
        self.add_widget(self.buttonmenu)

        #self.mapspace = MapScreen_List(self.app)
        #self.add_widget(self.mapspace)

        map = MapScreen_List(self.app)
        mapspace = ScrollView()
        mapspace.add_widget(Button())
        self.mapspace = mapspace
        self.add_widget(mapspace)


    def refresh(self, message):
        
        for item in self.mapspace.children:

            item.parse_message(message)

    def clear(self):

        try:
            self.remove_widget(self.addmenu)
            self.addmenu = None
        
        except:
            pass

        try:
            self.remove_widget(self.mapspace)

        except:
            pass


    def show_mapspace(self):

        self.add_widget(self.mapspace)
        self.app.state.menu = None



#MapScreen Menus

class MapScreen_ButtonMenu(BoxLayout):

    def __init__(self, *args):

        self.app = args[0]

        super().__init__(   size_hint = [0.14, 1],
                            orientation = 'vertical')

        self.add_widget(Button())

        add_button = Button(text = 'Add')
        add_button.on_release = self.on_addbutton
        self.add_button = add_button
        self.add_widget(add_button)

        self.add_widget(Button())
        self.add_widget(Button())

        self.list_button = Button(text = 'List')
        self.add_widget(self.list_button)

    
    def on_addbutton(self):

        if self.app.state.menu != 'addmenu':
            self.app.root.mapscreen.clear()
            addmenu = MapScreen_AddMenu(self.app)
            self.app.root.mapscreen.addmenu = addmenu
            self.app.root.mapscreen.add_widget(addmenu)
            self.app.state.menu = 'addmenu'

        else:
            self.app.root.mapscreen.clear()
            self.app.root.mapscreen.show_mapspace()
            self.app.state.menu = None


class MapScreen_AddMenu(BoxLayout):

    def __init__(self, app):

        self.app = app
        super().__init__(   orientation = 'vertical',
                            size_hint = (0.86, 1))

        bar = Label(text = 'Add sensor',
                    size_hint = (1, 0.1))
        self.bar = bar
        self.add_widget(bar)

        name_label = Label( text = 'Name',
                            size_hint = (1, 0.15))
        self.name_label = name_label
        self.add_widget(name_label)

        name_input = TextInput(size_hint = (1, 0.15))
        self.name_input = name_input
        self.add_widget(name_input)

        ID_label = Label(   text = 'ID',
                            size_hint = (1, 0.15))
        self.ID_label = ID_label
        self.add_widget(ID_label)

        ID_input = TextInput(size_hint = (1, 0.15))
        self.ID_input = ID_input
        self.add_widget(ID_input)

        OK_button = Button(text = 'Add')
        OK_button.on_release = self.on_okbutton
        self.OK_button = OK_button

        cancel_button = Button(text = 'Cancel')
        cancel_button.on_release = self.on_cancelbutton
        self.cancel_button = cancel_button

        buttonbox = BoxLayout(orientation = 'horizontal',
                                size_hint = (1, 0.2))
        buttonbox.add_widget(OK_button)
        buttonbox.add_widget(cancel_button)
        self.add_widget(buttonbox)


    def on_cancelbutton(self):

        #self.app.root.mapscreen.remove_widget(self)
        #self.app.root.mapscreen.addmenu = None
        self.app.root.mapscreen.clear()

        self.app.root.mapscreen.add_widget(self.app.root.mapscreen.mapspace)
        self.app.state.menu = None

    def on_okbutton(self):

        self.app.root.mapscreen.remove_widget(self)
        self.app.root.mapscreen.addmenu = None

        name = self.name_input.text
        ID = self.ID_input.text

        new_entry = MapScreen_ListEntry(name, ID)
        self.app.root.mapscreen.mapspace.add_widget(new_entry)
        self.app.root.mapscreen.add_widget(self.app.root.mapscreen.mapspace)

        self.app.state.menu = None



class MapScreen_List(StackLayout):

    def __init__(self, app):

        self.app = app

        super().__init__(   size_hint = [0.86, 1],
                            orientation = 'lr-tb',
                            spacing = 2)


class MapScreen_ListEntry(BoxLayout):

    def __init__(self, name, ID):

        super().__init__(   size_hint = [1, 0.1],
                            orientation = 'horizontal')

        self.name = name
        self.ID = ID

        #idle - no communication acquired until now
        #active - received activity signal
        #inactive - received inactivity signal
        self.state = 'idle'

        small_button = Button(  color = [0, 255, 0],
                                size_hint = [0.1, 1])
        small_button.background_color = [0.5, 0.5, 0.5, 255]
        self.small_button = small_button
        self.add_widget(small_button)

        big_button = Button(size_hint = [0.8, 1],
                            text = name)
        self.add_widget(big_button)
        big_button.background_color = [1.5, 1.5, 2, 1]
        self.big_button = big_button

        delete_button = Button( size_hint = [0.1, 1],
                                text = 'X')
        delete_button.background_normal = ''
        delete_button.background_color = [0.2, 0, 0, 1]
        delete_button.on_release = self.delete
        self.delete_button = delete_button
        self.add_widget(delete_button)

        #big_button.on_release = self.test


    def parse_message(self, message):

        if self.ID + ' active' in message:
            self.small_button.background_color = (0, 1, 0, 1)

        elif self.ID + ' inactive' in message:
            self.small_button.background_color = (1, 0, 0, 1)

        elif not self.ID in message:
            self.small_button.background_color = (0.5, 0.5, 0.5, 1)
 

    def delete(self):

        self.parent.remove_widget(self)


    @mainthread
    def callback(self, *args):
        print('The request succeeded!',
        'This callback is called in the main thread.')




if False:
    class StartScreen_ButtonMenu(FloatLayout):

        def __init__(self, *args):

            self.app = args[0]

            super().__init__(size_hint = cfg['button size hint'])


            button_box = BoxLayout( size_hint = (cfg['startscreen button width'], 1),
                                    orientation = 'vertical',
                                    pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

            start_button = Button(  text = 'Start',
                                    background_normal = cfg['button default'],
                                    background_down = cfg['button down'],
                                    font_size = 18,
                                    bold = False,
                                    color = (1, 1, 1, 1))

            start_button.on_release = self.app.show_connscreen
            button_box.add_widget(start_button)

            connbutton = Button(text = 'Credentials',
                                background_normal = cfg['button default'],
                                background_down = cfg['button down'],
                                font_size = 18,
                                bold = False)
            connbutton.on_release = self.app.open_connmenu
            button_box.add_widget(connbutton)

            infobutton = Button(text = 'Info',
                                background_normal = cfg['button default'],
                                background_down = cfg['button down'],
                                font_size = 18,
                                bold = False)
            infobutton.on_release = self.app.open_infomenu
            button_box.add_widget(infobutton)

            self.add_widget(button_box)


    class StartScreen_ConnMenu(BoxLayout):

        def __init__(self, *args):

            self.app = args[0]

            super().__init__(   size_hint = cfg['button size hint'], 
                                orientation = 'vertical', 
                                pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

            conndata = parse_connection_settings(self.app)

            self.user_subbox = cm_Subbox(   label = 'Username',
                                            text = conndata['user'])
            self.user_input = self.user_subbox.input

            self.pass_subbox = cm_Subbox(   label = 'Password',
                                            text = conndata['password'])
            self.pass_input = self.pass_subbox.input

            self.button_subbox = BoxLayout( size_hint = (1, 1),
                                            orientation = 'horizontal')
            accept_button = Button( text = 'Apply',
                                    background_color = (1, 1, 1),
                                    color = (1, 1, 1),
                                    background_normal = cfg['button default'],
                                    background_down = cfg['button down'],
                                    font_size = 18,
                                    bold = True)
            accept_button.on_release = self.on_save

            cancel_button = Button( text = 'Return',
                                    background_color = (1, 1, 1),
                                    color = (1, 1, 1),
                                    background_normal = cfg['button default'],
                                    background_down = cfg['button down'],
                                    font_size = 18,
                                    bold = True)
            cancel_button.on_release = self.app.clear_startscreen
            self.button_subbox.add_widget(accept_button)
            self.button_subbox.add_widget(cancel_button)

            self.add_widget(self.user_subbox)
            self.add_widget(self.pass_subbox)

            self.add_widget(self.button_subbox)


        def on_save(self):

            try:
                user = self.user_input.text
                password = self.pass_input.text

                s = str(user) + '\n'
                s = s + encrypt_password(password, cfg['DES key'])
                content = bytes(s, 'utf-8')

                if platform == 'android':
                    from android.storage import primary_external_storage_path
                    #fname = os.path.join(primary_external_storage_path(), cfg['connection file android'])
                    directory = self.app.user_data_dir
                    fname = os.path.join(directory, cfg['connection file android'])

                else:
                    fname = cfg['connection file']

                f = open(fname, 'wb')
                f.write(content)
                self.app.log('Credentials saved to ' + fname) 

            except:
                self.app.log('Error writing settings file.')

            self.app.clear_startscreen()


    class StartScreen_InfoMenu(BoxLayout):

        def __init__(self, *args):

            self.app = args[0]

            super().__init__(orientation = 'vertical',
                            size_hint = cfg['button size hint'])

            text = self.app.debuglog#cfg['app info']

            label =  Label( text = text,
                            size_hint = (1, 0.6666))

            button_padding = FloatLayout(size_hint = (1, 0.3333))
            button = Button(text = 'Return',
                            background_normal = cfg['button default'],
                            background_down = cfg['button down'],
                            font_size = 18,
                            bold = False,
                            size_hint = (cfg['startscreen button width'], 1),
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})
            button.on_release = self.app.clear_startscreen

            button_padding.add_widget(button)
            self.add_widget(label)
            self.add_widget(button_padding)

    # ConnMenu extras
    class cm_TextInput(TextInput):

        move = True
        focusek = False

        def on_focus(self, instance, value):

            app = self.parent.parent.parent.app
            if platform == 'android' or cfg['android mode']:

                if self.focused:
                    anim = Animation(duration = 0.4, pos_hint = {'center_y' : 0.85, 'center_x' : 0.5})
                    anim.start(app.root.startscreen)
                    if cm_TextInput.focusek: cm_TextInput.move = False
                    cm_TextInput.focusek = True

                else:
                    if cm_TextInput.move:
                        anim = Animation(duration = 0.4, pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})
                        anim.start(app.root.startscreen)
                        cm_TextInput.focusek = False

                    cm_TextInput.move = True
            

    #inputs kwargs: label, text
    class cm_Subbox(BoxLayout):

        def __init__(self, **kwargs):

            super().__init__(orientation = 'vertical')

            self.label = Label( text = kwargs['label'],
                                size_hint = (1, 0.5),
                                valign = 'middle',
                                halign = 'center',
                                font_size = 18,
                                bold = True,
                                color = (1, 1, 1))
            self.add_widget(self.label)

            self.input = cm_TextInput(  text = kwargs['text'],
                                        background_color = (0.1, 0.1, 0.1),
                                        foreground_color = (1, 1, 1),
                                        font_size = 15,
                                        halign = 'center',
                                        multiline = False)
            self.add_widget(self.input)

