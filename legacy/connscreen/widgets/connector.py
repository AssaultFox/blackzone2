from typing import Text
from kivy import app
from kivy.app import App
from kivy.core import image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.graphics import Color, Ellipse, Line, Rectangle, Triangle
from kivy.app import App
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.utils import platform
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.animation import Animation
from kivy.uix.scatter import Scatter
from kivy.uix.label import Label
from kivy.animation import Animation, AnimationTransition
from kivy.clock import Clock
#from blackwidgets import Window
import paho.mqtt.client as mqtt
from pyDes import *
import os
from config import cfg
from general import *


class ConnScreen_Connector(FloatLayout):

    def __init__(self, *args):

        self.app = args[0]
        super().__init__()

        Clock.schedule_once(self.connect, cfg['slide duration'])

        self.banner = Image(source = cfg['logo banner'],
                            color = (1, 1, 1, 1),
                            pos_hint = {'center_y' : 1 - 0.5 * cfg['banner size hint'][1], 'center_x' : 0.5},
                            size_hint = cfg['banner size hint'])
        self.slide = Animation( duration = cfg['slide duration'],
                                pos_hint = {'center_y' : 0.5, 'center_x' : 0.5},
                                transition = AnimationTransition.in_out_quart)
        self.slide.start(self.banner)
        self.add_widget(self.banner)

        self.status = Label( text = 'Connecting...',
                        size_hint = (1, 1 - cfg['banner size hint'][1]),
                        pos_hint = {'center_y' : 0.5 * cfg['banner size hint'][1], 'center_x' : 0.5},
                        font_size = 18)

        self.add_widget(self.status)


def decrypt_password(hash, key):

    stringlist = hash.split('-')
    intlist = []

    for item in stringlist:
        
        intlist.append(int(item))

    bytelist = bytes(intlist)
    password = triple_des('weednoseweednose').decrypt(bytelist, padmode=2)

    return password


def parse_connection_settings():

    cfg = APP.cfg

    if platform == 'android':
        from android.storage import primary_external_storage_path
        fname = os.path.join(primary_external_storage_path(), cfg['connection file android'])

    else:
        fname = cfg['connection file']

    f = open(fname, 'r')

    content = f.read()
    print(content)

    s = content.split('\n')

    return {'user': s[0], 'password': decrypt_password(s[1], None), 'server': s[2], 'port': s[3]}


def back_to_startscreen(dt):

    APP.show_startscreen()


def connect(dt):

    try:
        dict = parse_connection_settings()

        APP.client = mqtt.Client()
        APP.broker = dict['server']
        APP.port = int(dict['port'])
        APP.thread = 'fox/status/'

    except:
        box.status.text = 'Failed to read connection file.'
        Clock.schedule_once(back_to_startscreen, 2)
        return None

    try:
        APP.client.on_connect = APP.on_connect
        APP.client.on_message = APP.on_message
        APP.client.on_disconnect = APP.on_disconnect
        APP.client.connect(APP.broker, int(APP.port), 60)
        APP.client.username_pw_set(username = 'Blackzone', password='Dangersite')
        APP.client.loop_start()
        APP.client.subscribe([(APP.thread, 1)])
        
        box.status.text = 'Connection successful.'

    except:
        box.status.text = 'Failed to connect using specified parameters.'
        Clock.schedule_once(back_to_startscreen, 2)


def build_ConnScreen_Connector(app):

    global ROOT
    global APP
    APP = app
    ROOT = app.root
    cfg = app.cfg

    global box
    box = FloatLayout()
    Clock.schedule_once(connect, cfg['slide duration'])

    banner = Image( source = cfg['logo banner'],
                    color = (1, 1, 1, 1),
                    pos_hint = {'center_y' : 1 - 0.5 * cfg['banner size hint'][1], 'center_x' : 0.5},
                    size_hint = cfg['banner size hint'])
    slide = Animation(  duration = cfg['slide duration'],
                        pos_hint = {'center_y' : 0.5, 'center_x' : 0.5},
                        transition = AnimationTransition.in_out_quart)
    slide.start(banner)
    box.add_widget(banner)

    status = Label( text = 'Connecting...',
                    size_hint = (1, 1 - cfg['banner size hint'][1]),
                    pos_hint = {'center_y' : 0.5 * cfg['banner size hint'][1], 'center_x' : 0.5},
                    font_size = 18)
    box.status = status
    box.add_widget(status)

    return box