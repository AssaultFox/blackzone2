from typing import Text
from kivy import app
from kivy.app import App
from kivy.core import image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.graphics import Color, Ellipse, Line, Rectangle, Triangle
from kivy.app import App
from kivy.uix.filechooser import FileChooserIconView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.utils import platform
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.clock import mainthread
from kivy.uix.label import Label


### WIDGET CLASSES

class BlackzoneSensor(Button):

    def __init__(self, **kwargs):
        
        super().__init__(**kwargs)
        self.active = False    


    def refresh(self, message):
        
        if self.address in message:
            if message == self.address + '-activate':
                self.active = True
                self.draw_active()

            elif message == self.address + '-deactivate':
                self.active = False
                self.draw_inactive()


    def draw_first(self, pos):
        
        with self.canvas:
            
            self.canvas.clear()
            Hexagon(self, pos, size = 65, color = [0, 0, 0, 0.5])
            Hexagon(self, pos, size = 61, color = [0.5, 0.5, 0.5, 0.5])
            Hexagon(self, pos, size = 60, color = [1, 1, 1, 0.5])


    def draw_active(self):

        with self.canvas:

            self.canvas.clear()
            Hexagon(self, (self.center_x, self.center_y), size = 65, color = [0, 0, 0, 0.5])
            Hexagon(self, (self.center_x, self.center_y), size = 61, color = [1, 1, 0, 0.5])
            Hexagon(self, (self.center_x, self.center_y), size = 60, color = [2, 2, 0, 0.5])


    def draw_inactive(self):

        with self.canvas:

            self.canvas.clear()
            print(self.center_x)
            Hexagon(self, (self.center_x, self.center_y), size = 65, color = [0, 0, 0, 0.5])
            Hexagon(self, (self.center_x, self.center_y), size = 61, color = [0.25, 0.25, 0.25, 0.5])
            Hexagon(self, (self.center_x, self.center_y), size = 60, color = [0.5, 0.5, 0.5, 0.5])


    def on_press(self):
        
        print('Pressed!')
        super().on_press()


    def set_address(self, adr = None):
        self.address = adr


class ScreenSpace(FloatLayout):

    def generic(self):
        pass


class MapScreen(BoxLayout):

    def refresh(self, message):

        map.refresh(message)


class BlackzoneMap(FloatLayout, Image):

    locked = True

    @mainthread
    def refresh(self, message):
        
        for item in self.children:
            
            item.refresh(message)


    def add_sensor(self, address, pos):

        print('Adding a sensor to the map.')

        #sensor = BlackzoneSensor(text = '',
         #                           size_hint = (0.05, 0.05),
           #                         pos_hint = )

    def on_touch_down(self, touch):
        
        if not self.locked:
                
            xr = (touch.spos[0] - 0.15) / 0.85
            yr = touch.spos[1]


            if self.add_active == True and self.collide_point(*touch.pos):

                btn = BlackzoneSensor(          text ='', 
                                                size_hint = (0.05, 0.05), 
                                                pos_hint = {'center_x': xr, 'center_y': yr})
                
                btn.background_color = [0, 0, 0, 1]

                btn.set_address(self.add_ID)

                self.add_active = False
                self.add_widget(btn, canvas = 'after', index = 1000)
                btn.draw_first((touch.x, touch.y))
                print(touch.x, touch.y, btn.center_x, btn.center_y)

            for item in self.children:
                item.on_touch_down(touch)

            else:
                pass


    def on_reset(self):

        self.clear_widgets()


class Window(BoxLayout, Image):

    def generic(self):
        pass


def Hexagon(widget, pos, size, color = [0, 1, 0, 1]):

    x = pos[0]
    y = pos[1]
    a = size / 2
    ptp2 = 1.732051 / 2

    x1 = x + a / 2
    y1 = y + ptp2 * a

    x2 = x + a
    y2 = y

    x3 = x + a / 2
    y3 = y - ptp2 * a

    x4 = x - a / 2
    y4 = y - ptp2 * a

    x5 = x - a
    y5 = y

    x6 = x - a / 2
    y6 = y + ptp2 * a
    
    with widget.canvas:
        
        Color(color[0], color[1], color[2], color[3])
        Triangle(points = (x, y, x1, y1, x2, y2))
        Triangle(points = (x, y, x2, y2, x3, y3))
        Triangle(points = (x, y, x3, y3, x4, y4))
        Triangle(points = (x, y, x4, y4, x5, y5))
        Triangle(points = (x, y, x5, y5, x6, y6))
        Triangle(points = (x, y, x6, y6, x1, y1))