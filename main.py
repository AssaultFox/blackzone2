from kivy.app import App
from kivy.utils import platform
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.filechooser import FileChooserIconView 
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.core.window import Window
from kivy.clock import Clock
import config
from kivy.clock import mainthread
import paho.mqtt.client as mqtt
from kivy.base import EventLoop

from blz_libs import *

#TODO: fix usage of self.state to handle state
#TODO: resize how far the menu goes
#TODO: resize fonts

### MOTHER CLASS
class Blackzone(App):

    # MQTT 
    #client = mqtt.Client()
    #broker = 'broker.hivemq.com'
    #port = '1883'
    #thread = 'fox/status/'

    #old
    def redefine_mqtt(self, broker, thread, port):

        self.on_pause()
        self.broker = broker
        self.thread = thread
        self.port = port
        self.on_resume()


    #old
    def on_start(self):

        EventLoop.window.bind(on_keyboard=self.on_keyboard)

        class empty():
            
            def __init__(self):
                pass

        self.state = empty()
        self.state.screen = 'helloscreen'
        self.state.menu = None
        self.state.connection = 'disconnected'

        pass

    #old
    def on_connect(self, client, userdata, flags, rc):

        if rc == 0:
            pass
            #print("Connection successful.")
    
        else:
            pass
            #print("Connection failed: %d\n", rc)

    #old
    def on_disconnect(self, client, userdata, rc):
    
        self.log('Client disconnected.')

    #new

    def tick(self, dt):

        for event in self.tick_events:
            
            try:
                event()

            except:
                pass

    @mainthread
    def on_message(self, client, userdata, message):

        message = message.payload.decode()

        self.log("Message Received: " + message)
        
        if self.state.screen == 'mapscreen':
            self.root.mapscreen.refresh(message)

    #old
    def on_pause(self):

        self.client.disconnect()
        return True
        
    #TODO: Add timeout behaviour
    def on_resume(self):
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect
        self.client.connect(self.broker,
                            int(self.port),
                            cfg['keepalive'])
        self.client.username_pw_set(username = self.username,
                                    password = self.password)
        self.client.loop_start()
        self.client.subscribe([(self.thread, 1)])
        

    def on_keyboard(self, window, key, *args):

        if key == 27:

            if self.state.screen == 'startscreen':
                
                self.clear_startscreen()

            if self.state.screen == 'connscreen':

                self.show_startscreen()
                
            return True #NOTE: Keeps app from closing


    # WINDOW HANDLING - opening and closing windows
    #NEW BATCH

    #MOVED
  #  def open_connmenu(self):

   #     try:
    #        self.root.startscreen.remove_widget(self.root.startscreen.buttonmenu)
     #       self.root.startscreen.buttonmenu = None
      #      self.root.startscreen.connmenu = StartScreen_ConnMenu(self)
       #     self.root.startscreen.add_widget(self.root.startscreen.connmenu)

        #    self.state.menu = 'connmenu'

        #except:
         #   pass

    #not used
    #def close_connmenu(self):

     #   try:
      #      self.root.startscreen.remove_widget(self.root.startscreen.connmenu)
       #     self.root.startscreen.connmenu = None

        #    self.root.startscreen.buttonmenu = StartScreen_ButtonMenu(self)
         #   self.root.startscreen.add_widget(self.root.startscreen.buttonmenu)

          #  self.state.menu = 'buttonmenu'

        #except:
         #   pass

    #MOVED
   # def clear_startscreen(self):

    #    try:
     #       self.root.startscreen.remove_widget(self.root.startscreen.connmenu)
      #      self.root.startscreen.connmenu = None

       # except:
        #    pass

        #try:
         #   self.root.startscreen.remove_widget(self.root.startscreen.infomenu)
          #  self.root.startscreen.infomenu = None

        #except:
         #   pass

        #try:
         #   self.root.startscreen.remove_widget(self.root.startscreen.buttonmenu)
          #  self.root.startscreen.buttonmenu = None

        #except:
         #   pass 

        #self.root.startscreen.buttonmenu = StartScreen_ButtonMenu(self)
        #self.root.startscreen.add_widget(self.root.startscreen.buttonmenu)

        #self.state.menu = 'buttonmenu'

    #MOVED
    #def open_infomenu(self):

     #   try:
      #      self.root.startscreen.remove_widget(self.root.startscreen.buttonmenu)
       #     self.root.startscreen.buttonmenu = None

        #    self.root.startscreen.infomenu = StartScreen_InfoMenu(self)
         #   self.root.startscreen.add_widget(self.root.startscreen.infomenu)

        #except:
         #   pass

        #self.state.menu = 'infomenu'

    #not used
#    def close_infomenu(self):

 #       try:
  #          self.root.startscreen.remove_widget(self.root.startscreen.infomenu)
   #         self.root.startscreen.infomenu = None

    #        self.root.startscreen.buttonmenu = StartScreen_ButtonMenu(self)
     #       self.root.startscreen.add_widget(self.root.startscreen.buttonmenu)
            
      #      self.state.menu = 'infomenu'

       # except:
        #    pass


    # SCREENS

    def clear_screen(self):

        try:
            self.root.remove_widget(self.root.startscreen)
            self.root.startscreen = None
            pass

        except:
            pass

        try:
            self.root.remove_widget(self.root.mapscreen)
            self.root.mapscreen = None
            pass

        except:
            pass

        try:
            self.root.remove_widget(self.root.connscreen)
            self.root.connscreen = None
            pass

        except:
            pass

        try:
            self.root.remove_widget(self.root.helloscreen)
            self.root.helloscreen = None
            pass

        except:
            pass

        self.state.screen = None
        self.state.menu = None


    def show_helloscreen(self):

        self.clear_screen()
        self.root.helloscreen = HelloScreen(self)
        self.root.add_widget(self.root.helloscreen)

        self.state.screen = 'helloscreen'
        self.state.menu = None


    def show_startscreen(self):

        self.clear_screen()
        self.root.startscreen = StartScreen(self)
        self.root.add_widget(self.root.startscreen)

        self.state.screen = 'startscreen'
        self.state.menu = 'buttonmenu'


    def show_mapscreen(self):

        self.clear_screen()
        self.root.mapscreen = MapScreen(self)
        self.root.add_widget(self.root.mapscreen)

        self.state.screen = 'mapscreen'
        self.state.menu = None


    def show_connscreen(self):

        if self.state.screen == 'startscreen' and self.state.menu == 'buttonmenu':

            self.clear_screen()
            self.root.connscreen = ConnScreen(self)
            self.root.add_widget(self.root.connscreen)

            self.state.screen = 'connscreen'
            self.state.menu = None


    def log(self, text):

        time = '%.2f' % Clock.get_boottime()
        added = '[' + time + '] ' + text
        print(added)
        self.debuglog += '\n' + added 

        text = ''
        for line in self.debuglog.split('\n')[-6:]:

            text += line + '\n'

        self.debuglog = text[:-1]


    #BUILD
    def build(self):

        self.debuglog = '[' + str(Clock.get_boottime()) + "] App built.\n"

        self.tick_events = []
        Clock.schedule_interval(self.tick, cfg['tick interval'])

        #if platform == 'android':
         #   import android
          #  from android.permissions import request_permissions, Permission
           # request_permissions([Permission.READ_EXTERNAL_STORAGE, Permission.WRITE_EXTERNAL_STORAGE])

        screen = FloatLayout()

        screen.helloscreen = HelloScreen(self)
        screen.add_widget(screen.helloscreen)

        return screen

# Instantiate and run the kivy app

if __name__ == '__main__':

    Blackzone().run()