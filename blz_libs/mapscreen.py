from re import S
from typing import Text
from xmlrpc.client import MAXINT
from kivy.app import App
from kivy.utils import platform
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.textinput import TextInput
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.core.window import Window
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label
from config import cfg
import kivy #TODO do wyjebania
from kivy.animation import Animation, AnimationTransition
import os
from general import *
from kivy.uix.scrollview import ScrollView
from kivy.clock import Clock
from time import time as now
from kivy.clock import mainthread


class MapScreen(BoxLayout):

    def __init__(self, app):


        super().__init__(   size_hint = cfg['mapscreen size hint'], #TODO: different parameter in config
                            orientation = 'horizontal',
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

        self.app = app
        self.buttonmenu = MapScreen_ButtonMenu(self.app)
        self.add_widget(self.buttonmenu)


        mapspace = ScrollView(  size_hint = [0.86, None],
                                size=(Window.width, Window.height * cfg['menu size hint'][1]), #TODO: different parameter in config
                                scroll_timeout = 100)  #NOTE: changes lag of clicking

        def fun(a, b): #NOTE: dobrze by było gdyby działało ale nie musi

            print(self.height)
            self.height = b * cfg['menu size hint'][1]
            self.children[0].height = self.height
            print(self.height)

        Window.bind(height = fun)
        list = MapScreen_List(self.app)
        mapspace.layout = list
        mapspace.add_widget(list)
        self.mapspace = mapspace
        self.add_widget(mapspace)


    def refresh(self, message):
        
        lines = message.split(';')
        groups = []
        
        for line in lines:
            try:
                parts = line.split(' ')
                group = []
                group.append(parts[0])
                group.append(parts[1])
                groups.append(group)
            
            except:
                pass

        for entry in self.mapspace.layout.children:

            included = False

            for group in groups:

                if group[0] == entry.ID:
                    included = True
                    entry.parse(True, group[1])                

            if not included:
                entry.parse(False, 20 * 365 * 24 * 3600)
        #for item in self.mapspace.layout.children:

            #item.parse_message(message)


    def clear(self):

        self.app.state.menu = None
        
        try:
            self.remove_widget(self.addmenu)
            self.addmenu = None
        
        except:
            pass

        try:
            self.remove_widget(self.menu)
            self.menu = None

        except:
            pass

        try:
            self.remove_widget(self.mapspace)

        except:
            pass


    def show_mapspace(self):

        self.add_widget(self.mapspace)
        self.app.state.menu = None

#MapScreen Menus

class MapScreen_ButtonMenu(BoxLayout):

    def __init__(self, *args):

        self.app = args[0]

        super().__init__(   size_hint = [0.14, 1], #TODO parametrize in config
                            orientation = 'vertical')

        menubutton = Button(text = 'MENU')
        menubutton.on_release = self.on_menubutton
        self.menubutton = menubutton
        self.add_widget(menubutton)

        add_button = Button(text = 'Add')
        add_button.on_release = self.on_addbutton
        self.add_button = add_button
        self.add_widget(add_button)

        self.add_widget(Button(text = 'Presets'))

        self.list_button = Button(text = 'Console')
        self.add_widget(self.list_button)

    
    def on_addbutton(self):

        if self.app.state.menu != 'addmenu':
            self.app.root.mapscreen.clear()
            addmenu = MapScreen_AddMenu(self.app)
            self.app.root.mapscreen.addmenu = addmenu
            self.app.root.mapscreen.add_widget(addmenu)
            self.app.state.menu = 'addmenu'

        else:
            self.app.root.mapscreen.clear()
            self.app.root.mapscreen.show_mapspace()
            self.app.state.menu = None


    def on_menubutton(self):

        if self.app.state.menu != 'menu':
            self.app.root.mapscreen.clear()
            menu = MapScreen_MainMenu(self.app)
            self.app.root.mapscreen.menu = menu
            self.app.root.mapscreen.add_widget(menu)
            self.app.state.menu = 'menu'

        else:
            self.app.root.mapscreen.clear()
            self.app.root.mapscreen.show_mapspace()
            self.app.state.menu = None


class MapScreen_AddMenu(BoxLayout):

    def __init__(self, app):

        self.app = app
        super().__init__(   orientation = 'vertical',
                            size_hint = (0.86, 1))

        bar = Label(text = 'Add sensor',
                    size_hint = (1, 0.1))
        self.bar = bar
        self.add_widget(bar)

        name_label = Label( text = 'Name',
                            size_hint = (1, 0.15))
        self.name_label = name_label
        self.add_widget(name_label)

        name_input = TextInput(size_hint = (1, 0.15))
        self.name_input = name_input
        self.add_widget(name_input)

        ID_label = Label(   text = 'ID',
                            size_hint = (1, 0.15))
        self.ID_label = ID_label
        self.add_widget(ID_label)

        ID_input = TextInput(size_hint = (1, 0.15))
        self.ID_input = ID_input
        self.add_widget(ID_input)

        colorbox = BoxLayout(   orientation = 'horizontal',
                                size_hint = (1, 0.1))
        grey_button = ToggleButton( text = '',
                                    group = 'color',
                                    state = 'down',
                                    background_normal = 'img/buttons/new_button.png',
                                    background_down = 'img/buttons/new_button_up2.png',
                                    background_color = [1, 1, 1, 1])
        self.grey_button = grey_button

        blue_button = ToggleButton( text = '',
                                    group = 'color',
                                    background_normal = 'img/buttons/new_button.png',
                                    background_down = 'img/buttons/new_button_up2.png',
                                    background_color = [1, 1, 1.5, 1])
        self.blue_button = blue_button

        yellow_button = ToggleButton(   text = '',
                                        group = 'color',
                                        background_normal = 'img/buttons/new_button.png',
                                        background_down = 'img/buttons/new_button_up2.png',
                                        background_color = [1.5, 1.5, 1, 1])
        self.yellow_button = yellow_button

        green_button = ToggleButton(text = '',
                                    group = 'color',
                                    background_normal = 'img/buttons/new_button.png',
                                    background_down = 'img/buttons/new_button_up2.png',
                                    background_color = [1, 1.5, 1, 1])
        self.green_button = green_button

        colorbox.add_widget(grey_button)
        colorbox.add_widget(blue_button)
        colorbox.add_widget(yellow_button)
        colorbox.add_widget(green_button)

        #print(kivy.__file__)

        self.add_widget(colorbox)        

        OK_button = Button(text = 'Add')
        OK_button.on_release = self.on_okbutton
        self.OK_button = OK_button

        cancel_button = Button(text = 'Cancel')
        cancel_button.on_release = self.on_cancelbutton
        self.cancel_button = cancel_button

        buttonbox = BoxLayout(orientation = 'horizontal',
                                size_hint = (1, 0.2))
        buttonbox.add_widget(OK_button)
        buttonbox.add_widget(cancel_button)
        self.add_widget(buttonbox)


    def on_cancelbutton(self):

        #self.app.root.mapscreen.remove_widget(self)
        #self.app.root.mapscreen.addmenu = None
        self.app.root.mapscreen.clear()

        self.app.root.mapscreen.add_widget(self.app.root.mapscreen.mapspace)
        self.app.state.menu = None

    def on_okbutton(self):

        if self.grey_button.state == 'down':
            color = self.grey_button.background_color

        if self.blue_button.state == 'down':
            color = self.blue_button.background_color

        elif self.yellow_button.state == 'down':
            color = self.yellow_button.background_color

        elif self.green_button.state == 'down':
            color = self.green_button.background_color

        else:
            color = self.grey_button.background_color

        self.app.root.mapscreen.remove_widget(self)
        self.app.root.mapscreen.addmenu = None

        name = self.name_input.text
        ID = self.ID_input.text

        index = len(self.app.root.mapscreen.mapspace.layout.children)

        new_entry = MapScreen_ListEntry(name, ID, color, index)
        self.app.root.mapscreen.mapspace.layout.add_widget(new_entry)
        self.app.root.mapscreen.add_widget(self.app.root.mapscreen.mapspace)
        
        new_entry.indicator.on_release = self.app.root.mapscreen.mapspace.layout.sort_by_recent

        self.app.state.menu = None


class MapScreen_MainMenu(BoxLayout):

    def __init__(self, app):

        self.app = app
        super().__init__(   orientation = 'vertical',
                            size_hint = (0.86, 1))

        app.state.menu = 'menu'

        bar = Label(text = 'Menu',
                    size_hint = (1, 0.1))
        self.bar = bar
        self.add_widget(bar)

        connbox = BoxLayout(orientation = 'horizontal',
                            size_hint = (1, 0.2))
        #
        connbutton = Button(size_hint = (0.2, 1))
        self.connbutton = connbutton
        #
        connbox2 = BoxLayout(   orientation = 'vertical',
                                size_hint = (0.8, 1))
        #
        conn_label = Label( text = 'Connected to blackzone.mqtt.com (fake)',
                            halign = 'left',
                            valign = 'center')
        conn_label.bind(size = conn_label.setter('text_size'))
        self.conn_label = conn_label
        #
        update_label = Label(   text = 'Last update 7 seconds ago (fake)',
                                halign = 'left',
                                valign = 'center')
        update_label.bind(size = update_label.setter('text_size'))
        self.update_label = update_label
        #
        connbox.add_widget(connbutton)
        connbox2.add_widget(conn_label)
        connbox2.add_widget(update_label)
        connbox.add_widget(connbox2)
        self.add_widget(connbox)

        activebox = BoxLayout(  orientation = 'horizontal',
                                size_hint = (1, 0.1))
        #
        active_checkbox = CheckBox( active = False,
                                    size_hint = (0.1, 1))
        self.active_checkbox = active_checkbox
        #
        active_label = Label(   text = 'Active timeout',
                                size_hint = (0.3, 1))
        #
        active_input = TextInput(size_hint = (0.3, 1))
        self.active_input = active_input
        #
        seconds_label = Label(  text = 'seconds',
                                size_hint = (0.3, 1))
        #
        activebox.add_widget(active_checkbox)
        activebox.add_widget(active_label)
        activebox.add_widget(active_input)
        activebox.add_widget(seconds_label)
        self.add_widget(activebox)

        recentbox = BoxLayout(  orientation = 'horizontal',
                                size_hint = (1, 0.1))
        #
        recent_checkbox = CheckBox( active = False,
                                    size_hint = (0.1, 1))
        self.recent_checkbox = recent_checkbox
        #
        recent_label = Label(   text = 'Sorting by recent',
                                size_hint = (0.9, 1),
                                halign = 'left',
                                valign = 'center')
        recent_label.bind(size=recent_label.setter('text_size')) #NOTE: required for actually successful text alignment
        #
        recentbox.add_widget(recent_checkbox)
        recentbox.add_widget(recent_label)
        self.add_widget(recentbox)

        sortbox = BoxLayout(orientation = 'horizontal',
                            size_hint = (1, 0.1))
        #
        sort_label = Label( text = 'Sort by: ',
                            size_hint = (0.25, 1),
                            halign = 'left',
                            valign = 'center')
        sort_label.bind(size=sort_label.setter('text_size'))
        #
        recentsort_button = Button( text = 'Recent',
                                    size_hint = (0.25, 1))
        self.recentsort_button = recentsort_button
        self.recentsort_button.on_release = self.app.root.mapscreen.mapspace.layout.sort_by_recent
        #
        namesort_button = Button(   text = 'Name',
                                    size_hint = (0.25, 1))
        self.namesort_button = namesort_button
        self.namesort_button.on_release = self.app.root.mapscreen.mapspace.layout.sort_by_name
        #
        colorsort_button = Button(  text = 'Color',
                                    size_hint = (0.25, 1))
        self.colorsort_button = colorsort_button
        self.colorsort_button.on_release = self.app.root.mapscreen.mapspace.layout.sort_by_color
        #
        sortbox.add_widget(sort_label)
        sortbox.add_widget(recentsort_button)
        sortbox.add_widget(namesort_button)
        sortbox.add_widget(colorsort_button)
        self.add_widget(sortbox)

        padding = Label(size_hint = (1, 0.2))
        self.add_widget(padding)

        okbutton = Button(  text = 'OK',
                            size_hint = (1, 0.2))
        okbutton.on_release = self.on_okbutton
        self.okbutton = okbutton
        self.add_widget(okbutton)



    def on_okbutton(self):

        self.app.root.mapscreen.remove_widget(self)
        self.app.root.mapscreen.menu = None
        self.app.root.mapscreen.add_widget(self.app.root.mapscreen.mapspace)
        
        self.app.state.menu = None



class MapScreen_List(StackLayout): 

    def __init__(self, app):

        self.app = app

        super().__init__(   orientation = 'lr-tb',
                            spacing = -cfg['listing height'], 
                            size_hint_y = None,
                            padding = [0, -cfg['listing height'], 0, cfg['listing height']])

        self.bind(minimum_height=self.setter('height'))
        
        self.height = self.minimum_height


    def add_widget(self, widget, index = None):

        widget.size_hint_y = None

        if index == None:
            super().add_widget(widget, index = len(self.children))

        else:
            super().add_widget(widget, index)
        #self.height = self.minimum_height

        try: 
            if widget.tick not in self.app.tick_events:
                self.app.tick_events.append(widget.tick) #TODO skonczyc

        except:
            pass

        print('List now has ' + str(len(self.children)) + ' entries.')
        print(self.app.tick_events)


    def sort_by_recent(self):

        tbs = []

        for child in self.children:

            tbs.append([child.time, child])

        tbs.sort(key = lambda x: -x[0])

        self.clear_widgets()

        i = 0

        for item in tbs:

            item[1].index = i
            self.add_widget(item[1])
            i = i+1


    def sort_by_name(self):

        tbs = []

        for child in self.children:

            tbs.append([child.name, child])

        def key(x):

            val = 0

            try:
                val = val - 1e6 * ord(x[0][0])

            except:
                pass

            try:
                val = val - 1e3 * ord(x[0][1])

            except:
                pass

            try:
                val = val - ord(x[0][2])

            except:
                pass

            print(val)
            return val


        tbs.sort(key = key)

        self.clear_widgets()

        i = 0

        for item in tbs:

            item[1].index = i
            self.add_widget(item[1])
            i = i+1


    def sort_by_color(self):

        tbs = []

        for child in self.children:

            tbs.append([child.color, child])

        tbs.sort(key = lambda x: 1e8 *x[0][0] + 1e4 * x[0][1] + x[0][2])

        self.clear_widgets()

        i = 0

        for item in tbs:

            item[1].index = i
            self.add_widget(item[1])
            i = i+1


class MapScreen_ListEntry_Base(BoxLayout):


    def move_to(self, index):

        if index == self.index:
            return 0

        elif index < len(self.parent.children) and index >= 0:
            i = self.index
            parent = self.parent
            A = self.parent.children[index].indicator.parent #can't refer to a list entry
            B = self

            parent.remove_widget(A)
            parent.add_widget(Label(text = 'Processing...'), index = index)

            parent.remove_widget(B)
            parent.add_widget(Label(text = 'Processing...'), index = i)

            #A.big_button.text = str(i)
            #B.big_button.text = str(i + 1)

            A.index = i
            parent.remove_widget(parent.children[i])
            parent.add_widget(A, index = i)

            B.index = index
            parent.remove_widget(parent.children[index])
            parent.add_widget(B, index = index)
            #print(parent.children)


    def move_up(self):#, button):

        if self.index < len(self.parent.children) - 1:
            i = self.index
            parent = self.parent
            A = self.parent.children[i + 1].indicator.parent #can't refer to a list entry
            B = self

            parent.remove_widget(A)
            parent.add_widget(Label(text = 'Processing'), index = i + 1)

            parent.remove_widget(B)
            parent.add_widget(Label(text = 'Processing...'), index = i)

            #A.big_button.text = str(i)
            #B.big_button.text = str(i + 1)

            A.index = i
            parent.remove_widget(parent.children[i])
            parent.add_widget(A, index = i)

            B.index = i + 1
            parent.remove_widget(parent.children[i + 1])
            parent.add_widget(B, index = i + 1)


    def move_down(self):#, button):

        if self.index > 0:

            i = self.index
            parent = self.parent
            A = self.parent.children[i - 1].indicator.parent
            B = self

            parent.remove_widget(A)
            parent.add_widget(Label(text = 'Processing'), index = i - 1)

            parent.remove_widget(B)
            parent.add_widget(Label(text = 'Processing...'), index = i)

            #A.big_button.text = str(i)
            #B.big_button.text = str(i - 1)

            A.index = i
            parent.remove_widget(parent.children[i])
            parent.add_widget(A, index = i)

            B.index = i -1
            parent.remove_widget(parent.children[i - 1])
            parent.add_widget(B, index = i - 1)


class MapScreen_ListEntry(MapScreen_ListEntry_Base):

    def __init__(self, name, ID, color, index):

        super().__init__(   size_hint = [1, None],
                            orientation = 'horizontal')

        self.name = name
        self.ID = ID
        self.color = color
        self.index = index
        self.time = 20 * 365 * 24 * 3600 #initialize as dead
        self.last = now() - self.time

        #idle - no communication acquired until now
        #active - received activity signal
        #inactive - received inactivity signal
        self.state = 'idle'

        indicator = Button( color = [0, 255, 0],
                            size_hint = [0.2, None])
        indicator.background_color = [0.5, 0.5, 0.5, 255]
        self.indicator = indicator
        self.add_widget(indicator)
            

        name_bar = Button(  size_hint = [0.5, None],
                            text = name)
        name_bar.background_color = color 
        self.name_bar = name_bar                  #TODO: add sorting: by color, by recent
        self.add_widget(name_bar)
        #big_button.background_color = [2, 2, 2.35, 1] #TODO: put list of colors to config
        #big_button.background_color = [2, 2.35, 2, 1] 


        status_box = BoxLayout(size_hint = [0.4, None])
        self.status_box = status_box
        self.add_widget(status_box)

        time_label = Label( text = '',
                            size_hint = [1, None])
        self.time_label = time_label
        status_box.add_widget(time_label)


        menu = BoxLayout(size_hint = [1, None])
        #
        up_button = Button(text = 'UP')
        up_button.on_release = self.move_up
        self.up_button = up_button
        menu.add_widget(up_button)
        #
        down_button = Button(text = '\/')
        down_button.on_release = self.move_down
        self.down_button = down_button
        menu.add_widget(down_button)
        #
        notifbutton = Button(text = 'N')  
        self.notifbutton = notifbutton
        menu.add_widget(notifbutton)
        #
        remove_button = Button(text = 'X')
        remove_button.background_color = [1, 0.4, 0.4, 1]
        remove_button.on_release = self.delete
        self.remove_button = remove_button
        menu.add_widget(remove_button)
        #
        self.menu = menu

        options_button = Button( size_hint = [0.1, None],
                                text = 'OPTS')
        options_button.on_release = self.on_options
        self.options_button = options_button
        self.add_widget(options_button)

        self.time_label.height = cfg['listing height']
        menu.height = cfg['listing height']

        for child in self.children:

            child.height = cfg['listing height']
        
        for child in menu.children:

            child.height = cfg['listing height']



    def parse(self, included, time_):

        if included:
            try:

                time = int(time_)
                days = int(time / 86400)
                hours = int((time - days * 86400) / 3600)
                minutes = int((time - days * 86400 - hours * 3600) / 60)
                seconds = int(time - days * 86400 - hours * 3600 - minutes * 60)

                timestring = str(seconds) + 's'
                if minutes > 0: timestring = str(minutes) + 'm ' + timestring
                if hours > 0: timestring = str(hours) + 'h ' + timestring
                if days > 0: timestring = str(days) + 'd ' + timestring

                self.time = time
                self.last = now() - time

                if time < 15:
                    self.indicator.background_color = (0, 2, 0, 1)

                else:
                    self.indicator.background_color = (1, 0, 0, 1)

            except:
                timestring = 'Error'
                self.indicator.background_color = (0.6, 0.6, 0.6, 1)


        elif not included:
            self.indicator.background_color = (0.6, 0.6, 0.6, 1)
            self.time = 20 * 365 * 24 * 3600
            self.last = now() - 20 * 365 * 24 * 3600
            timestring = 'Inactive'      

        self.time_label.text = timestring


    def delete(self):

        siblings = len(self.parent.children)
        parent = self.parent
        self.parent.remove_widget(self)
        parent.app.tick_events.remove(self.tick)

        for i in range(0, siblings - 1):
            parent.children[i].index = i


    @mainthread
    def callback(self, *args):
        print('The request succeeded!',
        'This callback is called in the main thread.')


    def on_options(self):
        
        if self.time_label in self.status_box.children:
            self.status_box.remove_widget(self.time_label)
            self.status_box.add_widget(self.menu)
        
        elif self.menu in self.status_box.children:
            self.status_box.remove_widget(self.menu)
            self.status_box.add_widget(self.time_label)

    
    def tick(self):

        time = int(now() - self.last)
        days = int(time / 86400)
        hours = int((time - days * 86400) / 3600)
        minutes = int((time - days * 86400 - hours * 3600) / 60)
        seconds = int(time - days * 86400 - hours * 3600 - minutes * 60)

        timestring = str(seconds) + 's'
        if minutes > 0: timestring = str(minutes) + 'm ' + timestring
        if hours > 0: timestring = str(hours) + 'h ' + timestring
        if days > 0: timestring = str(days) + 'd ' + timestring

        self.time = time
        self.time_label.text = timestring
