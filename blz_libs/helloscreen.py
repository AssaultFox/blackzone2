from kivy.app import App
from kivy.utils import platform
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.animation import Animation
from kivy.animation import AnimationTransition
from kivy.uix.floatlayout import FloatLayout
from config import cfg


class HelloScreen(FloatLayout):

    def __init__(self, app):

        self.app = app

        super().__init__(   size_hint = cfg['menu size hint'],
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

        self.logo = Image(   source = cfg['logo banner'],
                color = (1, 1, 1, 0),
                pos_hint = {'center_y' : 0.5, 'center_x' : 0.5},
                size_hint = cfg['banner size hint'])

        fade_in = Animation(duration = cfg['fade in duration'],
                            color = (1, 1, 1, 1)) 
        self.add_widget(self.logo)
        fade_in.start(self.logo)

        moveto_y = 1 - 0.5 * cfg['banner size hint'][1]
        slide = Animation(  duration = cfg['slide duration'],
                            pos_hint = {'center_y' : moveto_y, 'center_x' : 0.5},
                            transition = AnimationTransition.in_out_quart)
        move_logo = lambda dt: slide.start(self.logo)

        move_on = lambda dt: self.app.show_startscreen()

        Clock.schedule_once(move_logo, cfg['fade in duration'] + cfg['fade-slide interval'])
        move_on_time = cfg['fade in duration'] + cfg['fade-slide interval']
        move_on_time = move_on_time + cfg['slide duration'] + cfg['final freeze']
        Clock.schedule_once(move_on, move_on_time)
