from kivy.app import App
from kivy.utils import platform
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.core.window import Window
from config import cfg
from kivy.clock import Clock
from kivy.animation import Animation, AnimationTransition
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from general import *
import paho.mqtt.client as mqtt

class ConnScreen(BoxLayout):

    def __init__(self, app):

        self.app = app

        super().__init__(   size_hint = cfg['menu size hint'], 
                            orientation = 'vertical', 
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

        self.add_widget(ConnScreen_Connector(self.app))


class ConnScreen_Connector(FloatLayout):

    def __init__(self, *args):

        self.app = args[0]
        super().__init__()

        Clock.schedule_once(self.get_connect(), cfg['slide duration'])

        self.banner = Image(source = cfg['logo banner'],
                            color = (1, 1, 1, 1),
                            pos_hint = {'center_y' : 1 - 0.5 * cfg['banner size hint'][1], 'center_x' : 0.5},
                            size_hint = cfg['banner size hint'])
        self.slide = Animation( duration = cfg['slide duration'],
                                pos_hint = {'center_y' : 0.5, 'center_x' : 0.5},
                                transition = AnimationTransition.in_out_quart)
       # self.slide.start(self.banner)
        self.add_widget(self.banner)
        self.banner_down()

        self.status = Label( text = 'Connecting...',
                        size_hint = (1, 1 - cfg['banner size hint'][1]),
                        pos_hint = {'center_y' : 0.5 * cfg['banner size hint'][1], 'center_x' : 0.5},
                        font_size = 18)

        self.add_widget(self.status)

    
    def get_connect(self):

        def connect(dt):

            back_to_startscreen = lambda dt: self.app.show_startscreen()
            proceed_to_mapscreen = lambda dt: self.app.show_mapscreen()

            try:
                dict = parse_connection_settings(self.app)
                self.app.user = dict['user']
                self.app.password = dict['password']

                self.app.client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, client_id = self.get_id())
                self.app.broker = cfg['broker']
                self.app.port = cfg['port']
                self.app.topic = self.get_topic()

            except:
                self.status.text = 'Failed to read connection file.'
                self.app.log('Failed to read connection file.')
                self.banner_up()
                Clock.schedule_once(back_to_startscreen, 2)
                return None

            try:
                self.app.client.on_connect = self.app.on_connect
                self.app.client.on_message = self.app.on_message
                self.app.client.on_disconnect = self.app.on_disconnect
                self.app.client.connect(self.app.broker, 
                                        int(self.app.port), 
                                        cfg['keepalive'])
                self.app.client.username_pw_set(username = dict['user'], 
                                                password = dict['password'])
                self.app.client.loop_start()
                self.app.client.subscribe([(self.app.topic, 1)])
                
                self.status.text = 'Connection successful.'
                self.app.log('Connected to broker.')
                Clock.schedule_once(proceed_to_mapscreen, 1)


            except:
                self.status.text = 'Failed to connect using specified parameters'
                logtext = 'Failed to connect to ' + self.app.broker + ':'
                logtext += str(self.app.port) + ' as ' 
                logtext += self.app.user + '.'
                self.app.log(logtext)
                self.banner_up()

        return connect


    def get_topic(self):

        return cfg['default thread']


    def get_id(self):

        return 'Blackzone b5'

    
    def banner_down(self):

        anim = Animation(   duration = cfg['slide duration'],
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5},
                            transition = AnimationTransition.in_out_quart)
        anim.start(self.banner)

    
    def banner_up(self):

        anim = Animation(   duration = cfg['slide duration'],
                            pos_hint = {'center_y' : 1 - 0.5 * cfg['banner size hint'][1], 'center_x' : 0.5},
                            transition = AnimationTransition.in_out_quart)
        anim.start(self.banner)