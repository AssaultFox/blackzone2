from kivy.app import App
from kivy.utils import platform
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.uix.label import Label
from config import cfg
from kivy.animation import Animation, AnimationTransition
import os
from general import *


class StartScreen(BoxLayout):

    def __init__(self, app):

        self.app = app

        super().__init__(   size_hint = cfg['menu size hint'], 
                            orientation = 'vertical',
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})
        
        self.logo = Image(  source = 'img/blackzone_v3_small.png',
                            size_hint = cfg['banner size hint'])
        self.add_widget(self.logo)

        self.buttonmenu = StartScreen_ButtonMenu(self.app)
        self.add_widget(self.buttonmenu)

        self.buttonmenu.connbutton.on_release = self.open_connmenu
        self.buttonmenu.infobutton.on_release = self.open_infomenu

        self.app.state.menu = 'buttonmenu'


    def open_buttonmenu(self):
        
        try:
            self.clear()
            self.buttonmenu = StartScreen_ButtonMenu(self.app)
            self.add_widget(self.buttonmenu)

            self.buttonmenu.connbutton.on_release = self.open_connmenu
            self.buttonmenu.infobutton.on_release = self.open_infomenu

            self.app.state.menu = 'buttonmenu'

        except:
            pass


    def open_connmenu(self):

        try:
            self.remove_widget(self.buttonmenu)
            self.buttonmenu = None
            self.connmenu = StartScreen_ConnMenu(self.app)
            self.add_widget(self.connmenu)

            self.app.state.menu = 'connmenu'

        except:
            pass


    def open_infomenu(self):

        try:
            self.remove_widget(self.buttonmenu)
            self.buttonmenu = None

            self.infomenu = StartScreen_InfoMenu(self.app)
            self.add_widget(self.infomenu)
            self.app.state.menu = 'infomenu'

        except:
            pass


    def clear(self):

        try:
            self.remove_widget(self.connmenu)
            self.connmenu = None

        except:
            pass

        try:
            self.remove_widget(self.infomenu)
            self.infomenu = None

        except:
            pass

        try:
            self.remove_widget(self.buttonmenu)
            self.buttonmenu = None

        except:
            pass 

        self.app.state.menu = None


#StartScreen Menus
class StartScreen_ButtonMenu(FloatLayout):

    def __init__(self, *args):

        self.app = args[0]

        super().__init__(size_hint = cfg['button size hint'])


        button_box = BoxLayout( size_hint = (cfg['startscreen button width'], 1),
                                orientation = 'vertical',
                                pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

        start_button = Button(  text = 'Start',
                                background_normal = cfg['button default'],
                                background_down = cfg['button down'],
                                font_size = 18,
                                bold = False,
                                color = (1, 1, 1, 1))

        start_button.on_release = self.app.show_connscreen
        button_box.add_widget(start_button)

        connbutton = Button(text = 'Credentials',
                            background_normal = cfg['button default'],
                            background_down = cfg['button down'],
                            font_size = 18,
                            bold = False)
        self.connbutton = connbutton
        #connbutton.on_release = self.parent.open_connmenu
        button_box.add_widget(connbutton)

        infobutton = Button(text = 'Info',
                            background_normal = cfg['button default'],
                            background_down = cfg['button down'],
                            font_size = 18,
                            bold = False)
        self.infobutton = infobutton
        #infobutton.on_release = self.app.root.open_infomenu
        button_box.add_widget(infobutton)

        self.add_widget(button_box)


class StartScreen_ConnMenu(BoxLayout):

    def __init__(self, *args):

        self.app = args[0]

        super().__init__(   size_hint = cfg['button size hint'], 
                            orientation = 'vertical', 
                            pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})

        conndata = parse_connection_settings(self.app)

        self.user_subbox = cm_Subbox(   label = 'Username',
                                        text = conndata['user'])
        self.user_input = self.user_subbox.input

        self.pass_subbox = cm_Subbox(   label = 'Password',
                                        text = conndata['password'])
        self.pass_input = self.pass_subbox.input

        self.button_subbox = BoxLayout( size_hint = (1, 1),
                                        orientation = 'horizontal')
        accept_button = Button( text = 'Apply',
                                background_color = (1, 1, 1),
                                color = (1, 1, 1),
                                background_normal = cfg['button default'],
                                background_down = cfg['button down'],
                                font_size = 18,
                                bold = True)
        accept_button.on_release = self.on_save

        cancel_button = Button( text = 'Return',
                                background_color = (1, 1, 1),
                                color = (1, 1, 1),
                                background_normal = cfg['button default'],
                                background_down = cfg['button down'],
                                font_size = 18,
                                bold = True)
        cancel_button.on_release = self.app.root.startscreen.open_buttonmenu
        self.button_subbox.add_widget(accept_button)
        self.button_subbox.add_widget(cancel_button)

        self.add_widget(self.user_subbox)
        self.add_widget(self.pass_subbox)

        self.add_widget(self.button_subbox)


    def on_save(self):

        try:
            user = self.user_input.text
            password = self.pass_input.text

            s = str(user) + '\n'
            s = s + encrypt_password(password, cfg['DES key'])
            content = bytes(s, 'utf-8')

            if platform == 'android':
                from android.storage import primary_external_storage_path
                #fname = os.path.join(primary_external_storage_path(), cfg['connection file android'])
                directory = self.app.user_data_dir
                fname = os.path.join(directory, cfg['connection file android'])

            else:
                fname = cfg['connection file']

            f = open(fname, 'wb')
            f.write(content)
            self.app.log('Credentials saved to ' + fname) 

        except:
            self.app.log('Error writing settings file.')

        self.app.root.startscreen.open_buttonmenu()


class StartScreen_InfoMenu(BoxLayout):

    def __init__(self, *args):

        self.app = args[0]

        super().__init__(orientation = 'vertical',
                        size_hint = cfg['button size hint'])

        text = self.app.debuglog#cfg['app info']

        label =  Label( text = text,
                        size_hint = (1, 0.6666))

        button_padding = FloatLayout(size_hint = (1, 0.3333))
        button = Button(text = 'Return',
                        background_normal = cfg['button default'],
                        background_down = cfg['button down'],
                        font_size = 18,
                        bold = False,
                        size_hint = (cfg['startscreen button width'], 1),
                        pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})
        button.on_release = self.app.root.startscreen.open_buttonmenu

        button_padding.add_widget(button)
        self.add_widget(label)
        self.add_widget(button_padding)

# ConnMenu extras
class cm_TextInput(TextInput):

    move = True
    focusek = False

    def on_focus(self, instance, value):

        app = self.parent.parent.parent.app
        if platform == 'android' or cfg['android mode']:

            if self.focused:
                anim = Animation(duration = 0.4, pos_hint = {'center_y' : 0.85, 'center_x' : 0.5})
                anim.start(app.root.startscreen)
                if cm_TextInput.focusek: cm_TextInput.move = False
                cm_TextInput.focusek = True

            else:
                if cm_TextInput.move:
                    anim = Animation(duration = 0.4, pos_hint = {'center_y' : 0.5, 'center_x' : 0.5})
                    anim.start(app.root.startscreen)
                    cm_TextInput.focusek = False

                cm_TextInput.move = True
        

#inputs kwargs: label, text
class cm_Subbox(BoxLayout):

    def __init__(self, **kwargs):

        super().__init__(orientation = 'vertical')

        self.label = Label( text = kwargs['label'],
                            size_hint = (1, 0.5),
                            valign = 'middle',
                            halign = 'center',
                            font_size = 18,
                            bold = True,
                            color = (1, 1, 1))
        self.add_widget(self.label)

        self.input = cm_TextInput(  text = kwargs['text'],
                                    background_color = (0.1, 0.1, 0.1),
                                    foreground_color = (1, 1, 1),
                                    font_size = 15,
                                    halign = 'center',
                                    multiline = False)
        self.add_widget(self.input)

